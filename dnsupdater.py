#!/usr/bin/python3

import requests
import uuid
import time
import socket
import logging
import sys
import configparser
import os

#define base_path
base_path = os.path.dirname(os.path.realpath(__file__))
#init configparser
config = configparser.ConfigParser()
configfile =  os.path.join(base_path, 'dnsupdater.conf')
config.read(configfile)

# define log file name
filename_log = config['Logging']['logFileName']
logfile = os.path.join(base_path, filename_log)

#define ip filename
filename_s = config['Output']['fileName']
filename = os.path.join(base_path, filename_s)

#init logger
# https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# create a file handler
handler = logging.FileHandler(logfile)
handler.setLevel(logging.DEBUG)
# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(lineno)d - %(message)s')
handler.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(handler)

def main():
	try:
		ip = get_external_ip() #Get External IP
		if check_ip(ip) == 0:
			logger.debug("No Change needed: IP remain as: {}".format(ip))
		else:
			if update_dns(ip) == 1 :
				write_ip(ip)
	except:		
		logger.error(sys.exc_info()[0], exc_info=True)
		raise
	#get external IP

def get_external_ip():	
	url1 = config['ExternalIP']['URL'] # IP needs to be in text 127.0.0.1

	result = requests.get(url=url1)
	addr =  result.text
	logger.debug("{}".format(addr))
	#Check response is a valid IP
	# https://stackoverflow.com/questions/319279/how-to-validate-ip-address-in-python
	try:
		socket.inet_aton(addr)
		ip = addr
		logger.debug("{} looks like a valid IP".format(ip))
	except socket.error:
		logger.critical("{} doesn't looks like a valid IP. Terminating program".format(ip))
		quit()

	return(ip)

	
def check_ip(ip):
	try:
		with open(filename, 'r+') as f:
			if(f.read() == ip):
				logger.info("NO IP CHANGE \t{}".format(ip))
				return 0
			else:
				logger.info("IP CHANGE \t{}".format(ip))
				return 1
	except Exception as e: 
		logger.warning("IP File not found. A new one would be created \t{}".format(ip))
		return 1

def write_ip(ip):
	with open(filename, 'w+') as f:
		f.write(ip)
		logger.info("IP CHANGED ON FILE \t{}".format(ip))

def update_dns(ip):

	#DreamHost API
	key = config['Dreamhost']['KEY']

	url = config['Dreamhost']['API_URL'] #'https://api.dreamhost.com/'

	record = config['Dreamhost']['DNS_RECORD']
	value = ''

	#get list of existing DNS records
	result = requests.post(url=url, data = {'key':key, 'cmd':'dns-list_records', 'unique_id' : uuid.uuid4(), 'format' : 'json'})
	
	dnslist = result.json()
	#get value associated with record
	
	if dnslist['result'] == 'success':
		for entry in dnslist['data']:
			if entry['record'] == record :
				value = entry['value']
				logger.info("IP FOUND ON DNS {}".format(value))
				break
		if value == ip:
			logger.warning("IP FOUND ON DNS {} is equal to the current external one. IP file might have been manually modified".format(value))
			return 1
	else:
		logger.error('ERROR: {}'.format(dnslist))
		sys.exit('Error comunicating with Dreamhost')
		
	#Replace A type

	data_add = {'key':key, 
			'cmd':'dns-add_record', 
			'unique_id' : uuid.uuid4(),
			'record' : record,
			'type' : 'A',
			'value' : ip,
			'comment' : "{}".format(time.asctime())
			}

	data_rm = {'key':key, 
			'cmd':'dns-remove_record', 
			'unique_id' : uuid.uuid4(),
			'record' : record,
			'type' : 'A',
			'value' : value
			}		
	result = requests.post(url=url, data = data_rm)
	logger.debug("Removed entry: {}".format(result.text))

	result = requests.post(url=url, data = data_add)
	logger.debug("Added entry: {}".format(result.text))
	logger.info("UPDATED IP OF {}: {}".format(record,ip))
	return 1
		
if __name__ == '__main__':
    main()



